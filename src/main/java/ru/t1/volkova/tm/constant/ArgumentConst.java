package ru.t1.volkova.tm.constant;

public class ArgumentConst {

    public static final String ABOUT = "-a";

    public static final String VERSION = "-v";

    public static final String HELP = "-h";

}
